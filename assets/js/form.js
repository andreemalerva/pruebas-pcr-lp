/*var recaptcha = function () {
  grecaptcha.ready(function () {
    grecaptcha
      .execute("6LfsbNYUAAAAAIBBT6Z9mQmaXxoxZI5BfwZIoCbM", { action: "home" })
      .then(function (token) {
        document.getElementById("g-recaptcha-response-d").value = token;
        document.getElementById("g-recaptcha-response-m").value = token;
      });
  });
};*/

// recaptcha();
$(function () {
  // Your web app's Firebase configuration  // For Firebase JS SDK v7.20.0 and later, measurementId is optional
  var firebaseConfig = {
    apiKey: "AIzaSyBPfb0JjIxZrV_WUUKPycuhC6j5PvgcNuE",
    authDomain: "lp-olozfera.firebaseapp.com",
    databaseURL: "https://lp-olozfera.firebaseio.com",
    projectId: "lp-olozfera",
    storageBucket: "lp-olozfera.appspot.com",
    messagingSenderId: "986581798326",
    appId: "1:986581798326:web:c59f3efed908d60f250517",
    measurementId: "G-2W6T0VGX5X",
  };
  // Initialize Firebase
  firebase.initializeApp(firebaseConfig);

  /*---------------
            Validación
        ---------------*/

  $("form #telefono")
    .keypress(function (e) {
      if (isNaN(this.value + String.fromCharCode(e.charCode))) return false;
    })
    .on("cut copy paste", function (e) {
      e.preventDefault();
    });
  /*----- Habilitar boton solamente cuando este validado el formulario--*/
  $("form").on("change keyup", function () {
    var form = $(this);
    var btn = form.find("#enviar");

    if (validarFormulario(form)) {
      btn.attr("disabled", false);
      console.log("habilitado");
    } else {
      btn.attr("disabled", true);
      console.log("deshabilitado");
    }
  });

  var validarFormulario = function (form) {
    form.validate({
      rules: {
        nombre: { required: true },
        email: { required: true, email: true },
        telefono: {
          required: true,
          digits: true,
          minlength: 10,
          maxlength: 15,
        },
        ciudad: { required: true },
        horario: { required: true },
        medio: { required: true },
        prueba: { required: true },
      },

      messages: {
        nombre: {
          required: "Este campo es obligatorio",
        },
        email: {
          required: "Este campo es obligatorio",
          email: "Ingresa un email valido",
        },
        telefono: {
          required: "Este campo es obligatorio",
          digits: "Debes ingresar un número",
          minlength: "Debes ingresar al menos {0}",
          maxlength: "No debes ingresar más de {0}",
        },
        puesto: {
          required: "Este campo es obligatorio",
        },
        empresa: {
          required: "Este campo es obligatorio",
        },
        ciudad: {
          required: "Este campo es obligatorio",
        },
        horario: {
          required: "Este campo es obligatorio",
        },
        medio: {
          required: "Este campo es obligatorio",
        },
        prueba: {
          required: "Este campo es obligatorio",
        },
      },
    });

    return form.valid();
  };

  $("#enviar").click(function (e) {
    var form = $("#frm-d");
    if (validarFormulario(form)) {
      $(".loading").removeClass("d-none");
      $(".loading").addClass("d-flex");
      var datos = form.serializeObject();
      $("#enviar").attr("disabled", "true");
      let id = new Date();
      firebase
        .database()
        .ref("lpPcr/" + id)
        .set({
          nombre: datos.nombre,
          email: datos.email,
          telefono: datos.telefono,
          ciudad: datos.ciudad,
          horario: datos.horario,
          medio: datos.medio,
          prueba: datos.prueba,
          utmCampaign: datos.utm_campaign ? datos.utm_campaign : "null",
          utmContent: datos.utm_content ? datos.utm_content : "null",
          utmMedium: datos.utm_medium ? datos.utm_medium : "null",
          utmTerm: datos.utm_term ? datos.utm_term : "null",
          utmSource: datos.utm_source ? datos.utm_source : "null",
        });
     enviarExcel(datos, form);
    } else {
    }
  });

  $("#enviar-m").click(function (e) {
    var form = $("#frm-m");
    if (validarFormulario(form)) {
      $(".loading").removeClass("d-none");
      $(".loading").addClass("d-flex");
      var datos = form.serializeObject();
      $("#enviar-m").attr("disabled", "true");

      let id = new Date();
      firebase
        .database()
        .ref("lpPcr/" + id)
        .set({
          nombre: datos.nombre,
          email: datos.email,
          telefono: datos.telefono,
          ciudad: datos.ciudad,
          horario: datos.horario,
          medio: datos.medio,
          prueba: datos.prueba,
          utmCampaign: datos.utm_campaign ? datos.utm_campaign : "null",
          utmContent: datos.utm_content ? datos.utm_content : "null",
          utmMedium: datos.utm_medium ? datos.utm_medium : "null",
          utmTerm: datos.utm_term ? datos.utm_term : "null",
          utmSource: datos.utm_source ? datos.utm_source : "null",
        });
     // enviarExcel(datos, form);
    } else {
    }
  });

  /*    Formato Json
        ---------------------------*/
  $.fn.serializeObject = function () {
    var o = {};
    this.find("[name]").each(function () {
      o[this.name] = this.value;
    });
    return o;
  };

  var resultado = function (form, btn, msj) {
    $("#formuario .titulo").val(msj);
    form[0].reset();
    console.log(msj);
  };

  function enviarExcel(datos, form) {
    var url =
      "https://script.google.com/macros/s/AKfycbzi5C67Y6qc5CLDB_iVQIr4Ae97IoyQV1C1eRah-RKy3XZQeZM/exec";
    $.ajax({
      url: url,
      method: "GET",
      data: datos,
      dataType: "JSON",
      success: function (r) {
        console.log(r);
        form.submit();
      },
      error: function (xhr) {
        console.log(xhr.status);
        console.log(xhr.responseText);
        form.submit();
      },
    });
  }
});

/* OBTENER UTMS/**/

var obtener_utms = function (datos) {
  var utms = [
    "utm_source",
    "utm_medium",
    "utm_campaign",
    "utm_term",
    "utm_content",
  ];
  for (utm of utms) {
    datos[utm] = getParameterByName(utm);
  }

  return datos;
};

function getParameterByName(name) {
  name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
  var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
    results = regex.exec(location.search);
  return results === null
    ? "null"
    : decodeURIComponent(results[1].replace(/\+/g, " "));
}

var utms = [
  "utm_source",
  "utm_medium",
  "utm_campaign",
  "utm_term",
  "utm_content",
];

var utmFormulario = function () {
  let datos = {};
  datos = obtener_utms(datos);

  document.getElementById("utm_source").value = datos["utm_source"];
  document.getElementById("utm_content").value = datos["utm_content"];
  document.getElementById("utm_campaign").value = datos["utm_campaign"];
  document.getElementById("utm_term").value = datos["utm_term"];
  document.getElementById("utm_medium").value = datos["utm_medium"];
/*
  document.getElementById("utm_source-m").value = datos["utm_source"];
  document.getElementById("utm_content-m").value = datos["utm_content"];
  document.getElementById("utm_campaign-m").value = datos["utm_campaign"];
  document.getElementById("utm_term-m").value = datos["utm_term"];
  document.getElementById("utm_medium-m").value = datos["utm_medium"];*/

  console.log(datos);
};

utmFormulario();
