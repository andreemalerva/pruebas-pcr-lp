<?php

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

require 'assets/PHPMailer/src/Exception.php';
require 'assets/PHPMailer/src/PHPMailer.php';
require 'assets/PHPMailer/src/SMTP.php';
session_start();
function sendEmail($datos)
{

	$mensaje = "<p>Este mensaje viene de esta liga: https://covidtesting.mx/lp<p>";
	$mensaje .= "<ul>";
	$campos = array("nombre", "email", "telefono", "ciudad", "horario", "medio", "prueba",  "utm_source", "utm_medium", "utm_campaign", "utm_content", "utm_term");
	foreach ($campos as $value) {
		$mensaje .= "<li>$value: $datos[$value]</li>";
	}
	$mensaje .= "</ul>";

	// Instantiation and passing `true` enables exceptions
	$mail = new PHPMailer(true);
	try {
		$mail->SMTPDebug = 2;
		$mail->isSMTP();
		$mail->Host = 'smtp.gmail.com';
		$mail->SMTPAuth = true;
		$mail->Username = 'correosformularios@gmail.com';
		$mail->Password = '8j*BqMVSmP+%fD-S';
		$mail->SMTPSecure = 'ssl';
		$mail->Port = 465;
		$mail->setFrom('correosformularios@gmail.com', 'Solicitud de contacto - covidtestig ');
		//develop
		//$mail->addAddress('ejemplo@ejemplo.com');
		//production
		$mail->addAddress('ejemplo@ejemplo.com');
		$mail->Subject  = "Nuevo contacto covidtestig";
		$mail->Body =  $mensaje;
		$mail->IsHTML(true);
		$mail->send();
	} catch (Exception $exception) {
		// echo "Error: ". $exception->getMessage();
	}
}


function sendEmailSimple($datos)
{
	$to = "ejemplo@covidtesting.mx";
	$subject = "Nuevo contacto covidtestig frm | CDMX";

	$mensaje = "<p>Este mensaje viene de esta liga: https://covidtesting.mx/lp<p>";
	$mensaje .= "<ul>";
	$campos = array("nombre", "email", "telefono", "ciudad", "horario", "medio", "prueba", "utm_source", "utm_medium", "utm_campaign", "utm_content", "utm_term");
	foreach ($campos as $value) {
		$mensaje .= "<li>$value: $datos[$value]</li>";
	}
	$mensaje .= "</ul>";
	$headers = "MIME-Version: 1.0" . "\r\n";
	$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";

	$headers .= 'From: covidtesting <noreply@covidtesting.mx>' . "\r\n";
	$headers .= 'Cc: ejemplo@ejemplo.com' . "\r\n";
	$headers .= 'Cc: ejemplo@ejemplo.com' . "\r\n";
	mail($to, $subject, $mensaje, $headers);
}

function validarDatos($datos)
{
	foreach ($datos as $key => $value) {
		if ($value == '') {
			$_SESSION['error'] = "Todos los campos del formulario son obligatorios son obligatorios";
		}
	}
}

function limpiarDatos($datos)
{
	foreach ($datos as $key => $value) {
		$datos[$key] = trim($value);
		$datos[$key] = htmlspecialchars($value);
		$_SESSION[$key] = $value;
	}

	return $datos;
}

// if (isset($_POST['nombre'])) {
// 	$datos = $_POST;
// 	$datos = limpiarDatos($datos);
// 	validarDatos($datos);
// 	if (!isset($_SESSION['error'])) {
// 		try {
// 			session_destroy();
// 			sendEmailSimple($datos);
// 		} catch (\Throwable $th) {
// 			//throw $th;
// 		}
// 		header('location: gracias/');
// 	} else {
// 		header('location: /lp');
// 	}
// } else {
// 	header('location: /lp');
// }
$prueba1 = "PCR";
$prueba2 = "Prueba rápida de antígenos";
$prueba3 = "Prueba IgG";
$prueba4 = "Prueba rápida de sangre";

if ($_POST['prueba'] == $prueba1) {
	$datos = $_POST;
	$datos = limpiarDatos($datos);
	validarDatos($datos);
	if (!isset($_SESSION['error'])) {
		try {
			session_destroy();
			sendEmailSimple($datos);
		} catch (\Throwable $th) {
			//throw $th;
		}
		header('location: gracias-pcr');
	} else {
		header('location: /lp');
	}
} else if ($_POST['prueba'] == $prueba2) {
	$datos = $_POST;
	$datos = limpiarDatos($datos);
	validarDatos($datos);
	if (!isset($_SESSION['error'])) {
		try {
			session_destroy();
			sendEmailSimple($datos);
		} catch (\Throwable $th) {
			//throw $th;
		}
		header('location: gracias-pra');
	} else {
		header('location: /lp');
	}
} else if ($_POST['prueba'] == $prueba3) {
	$datos = $_POST;
	$datos = limpiarDatos($datos);
	validarDatos($datos);
	if (!isset($_SESSION['error'])) {
		try {
			session_destroy();
			sendEmailSimple($datos);
		} catch (\Throwable $th) {
			//throw $th;
		}
		header('location: gracias-pig');
	} else {
		header('location: /lp');
	}
} else if ($_POST['prueba'] == $prueba4) {
	$datos = $_POST;
	$datos = limpiarDatos($datos);
	validarDatos($datos);
	if (!isset($_SESSION['error'])) {
		try {
			session_destroy();
			sendEmailSimple($datos);
		} catch (\Throwable $th) {
			//throw $th;
		}
		header('location: gracias-prs');
	} else {
		header('location: /lp');
	}
} else {
	header('location: /lp');
}
