<?php
session_start();
$error;

$datos = null;
if (isset($_SESSION['error'])) {
	$error = $_SESSION['error'];
	$datos = $_SESSION;
	session_destroy();
}

function old($input, $datos)
{
	if (isset($datos)) {
		switch ($input) {
			case 'nombre':
				return $datos['nombre'];
				break;
			case 'empresa':
				return $datos['empresa'];
				break;
			case 'telefono':
				return $datos['telefono'];
				break;
			case 'email':
				return $datos['email'];
				break;
			case 'horario':
				return $datos['horario'];
				break;
			case 'medio':
				return $datos['medio'];
				break;
			default:
				# code...
				break;
		}
	}
}

?>
<!DOCTYPE html>
<html lang="es">

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta name="description" content="Pruebas COVID-19 en Mexico con recoleccion a domicilio y pago con tarjeta o deposito en OXXO">
	<meta name="author" content="SAFE Mexico">
	<link rel="canonical" href="https://covidtesting.mx">
	<meta name="robots" content="index, follow">
	<meta name=viewport content="width=device-width, initial-scale=1">

	<title>Análisis COVID-19 a domicilio</title>

	<!-- Favicon -->
	<link rel="shortcut icon" type="image/x-icon" href="../images/favicon.ico" />

	<!-- bootstrap.min css -->
	<link rel="stylesheet" href="https://covidtesting.mx/plugins/bootstrap/css/bootstrap.min.css?v=1">
	<!-- Icon Font Css -->
	<link rel="stylesheet" href="https://covidtesting.mx/plugins/icofont/icofont.min.css">
	<!-- Slick Slider  CSS -->
	<link rel="stylesheet" href="https://covidtesting.mx/plugins/slick-carousel/slick/slick.css">
	<link rel="stylesheet" href="https://covidtesting.mx/plugins/slick-carousel/slick/slick-theme.css">

	<!-- Main Stylesheet -->
	<link rel="stylesheet" href="https://covidtesting.mx/css/style.css">
	<!-- Global site tag (gtag.js) - Google Ads: 479402761 -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=AW-479402761"></script>
	<script>
		window.dataLayer = window.dataLayer || [];

		function gtag() {
			dataLayer.push(arguments);
		}
		gtag('js', new Date());
		gtag('config', 'AW-479402761');
	</script>

	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-47668237-2"></script>
	<script>
		window.dataLayer = window.dataLayer || [];

		function gtag() {
			dataLayer.push(arguments);
		}
		gtag('js', new Date());
		gtag('config', 'UA-47668237-2');
	</script>
	<!-- Google Tag Manager -->
	<script>
		(function(w, d, s, l, i) {
			w[l] = w[l] || [];
			w[l].push({
				'gtm.start': new Date().getTime(),
				event: 'gtm.js'
			});
			var f = d.getElementsByTagName(s)[0],
				j = d.createElement(s),
				dl = l != 'dataLayer' ? '&l=' + l : '';
			j.async = true;
			j.src =
				'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
			f.parentNode.insertBefore(j, f);
		})(window, document, 'script', 'dataLayer', 'GTM-WGBTV8D');
	</script>
	<!-- End Google Tag Manager -->


	<style type="text/css">
		.body-align {
			text-align: left;
		}

		.form-height {
			height: 720px;
		}

		.banner {
			min-height: 650px !important;
		}

		.input100 {
			width: 100%;
			height: 30px !important;
		}

		h1,
		h6,
		label {
			color: #223a66;
		}

		h2 {
			font-size: 20px;
			color: #223a66;
		}

		.form-container {
			background: #fff;
			z-index: 100;
			position: relative;
			padding: 15px;
			margin: 10px;
			border-radius: 15px 15px 15px 15px;
		}

		.texto01,
		.texto02,
		.texto03 {
			margin-top: 50px;
		}

		.carousel-control-prev {
			left: -18px;
		}

		.carousel-control-next {
			right: -18px;
		}

		.banner .block h1 {
			color: #fff;
		}

		.txtColorSlider {
			color: #fff;
		}

		.banner .block {
			padding: 0px 20px 20px 20px;
		}

		.error {
			margin-bottom: 0px;
			padding: 0px;
			font-size: 10px;
			color: #e12454;
		}

		label {
			margin: 0px;
		}

		#padre {
			display: table;
			height: 500px;
		}

		#hijo {
			display: table-cell;
			vertical-align: middle;
		}

		p {
			line-height: 20px;
		}

		/* .title-tab {
			text-align: center;
		} */

		/*@media (max-width: 10240px) {
			.form-height {
				height: 675px;
			}
		}*/

		@media (max-width: 540px) {
			#padre {
				display: table;
				height: 320px;
			}

			.input100 {
				width: 100%;
				height: 45px;
			}

			.form-height {
				height: 750px;
			}
		}
	</style>
</head>

<body id="top">
	<!-- Google Tag Manager (noscript) -->
	<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-WGBTV8D" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
	<!-- End Google Tag Manager (noscript) -->

	<header>
		<div class="header-top-bar">
			<div class="container">
				<div class="row align-items-center">
					<div class="col-lg-6">

					</div>
					<div class="col-lg-6">
						<div class="text-lg-right top-right-bar mt-2 mt-lg-0">

						</div>
					</div>
				</div>
			</div>
		</div>
		<nav class="navbar navbar-expand-lg navigation" id="navbar">
			<div class="container">
				<a class="navbar-brand" href="#">
					<img src="../images/logo.png" alt="PCR Investigation and Reference LOGO" class="img-fluid">
				</a>


				<h2 class="">Tel: 55 1823 9725</h2>

		</nav>
	</header>




	<!-- Slider Start -->
	<section class="banner">
		<div class="container">
			<div id="homeslider" class="row carousel slide" data-ride="carousel">
				<div class="col-lg-6 col-md-6 col-xl-6 carousel-inner txtColorSlider">

					<div id="padre">
						<div id="hijo">
							<h2>Realizamos pruebas PCR a domicilio.</h2>
							<h1>Laboratorio certificado ante el INDRE.</h1>
							<p style="    color: #223a66;">Entrega de 24 a 36 horas.<br>
								Contamos con todas las medidas de seguridad.<br>
								CDMX y área metropolitana. <br></p>

						</div>
					</div>


				</div>
				<div class="col-lg-6 col-md-6 col-xl-6 form-height">
					<!-- -->
					<div class="container">
						<div class="row">

							<div class="col-md-12">
								<div class="form-container">
									<h2>Déjanos tus datos para contactarnos contigo y agendar tu cita.</h2>
									<form id="frm-d" action="enviar.php" method="post">
										<label for="nombre">Nombre:</label><br>
										<input type="text" name="nombre" id="nombre" class="input100" value="<?php echo old("nombre", $datos); ?>" /><br>
										<label for="ciudad">Ciudad:</label><br>
										<input type="text" name="ciudad" id="ciudad" class="input100" value="<?php echo old("ciudad", $datos); ?>" /><br>
										<label for="correo">Correo:</label><br>
										<input type="email" name="email" id="email" class="input100" value="<?php echo old("email", $datos); ?>" /><br>
										<label for="telefono">Teléfono:</label><br>
										<input type="tel" name="telefono" id="telefono" class="input100" value="<?php echo old("telefono", $datos); ?>" /><br>
										<label for="horario">¿En qué horario quieres ser contactado?:</label><br>
										<input type="text" name="horario" id="horario" class="input100" value="<?php echo old("horario", $datos); ?>" /><br>
										<label for="medio">¿Por qué medio quieres ser contactado?:</label><br>
										<input type="text" name="medio" id="medio" class="input100" value="<?php echo old("medio", $datos); ?>" /><br>
										<label for="medio">¿Cuál es la prueba de tu interés?</label><br>
										<select class="input100" aria-label="¿Cuál es la prueba de tu interés?" id="prueba" name="prueba" required>
											<option value="">Selecciona una opción</option>
											<option value="PCR">PCR</option>
											<option value="Prueba rápida de antígenos">Prueba rápida de antígenos</option>
											<option value="Prueba IgG">Prueba IgG</option>
											<option value="Prueba rápida de sangre">Prueba rápida de sangre</option>
										</select>
										<br><br>
										<input type="hidden" name="utm_source" id="utm_source">
										<input type="hidden" name="utm_content" id="utm_content">
										<input type="hidden" name="utm_campaign" id="utm_campaign">
										<input type="hidden" name="utm_term" id="utm_term">
										<input type="hidden" name="utm_medium" id="utm_medium">
										<center><input type="button" value="ENVIAR" class="btn btn-main-2 btn-round-full" id="enviar"></center>

									</form>
								</div>
							</div>
						</div>
					</div>
					<!-- -->
				</div>
			</div>


		</div>
	</section>

	<!-- <section class="features">
		<div class="container">
			<div class="row">
				<div class="col-lg-12">
					<div class="feature-block d-lg-flex">

						<div class="feature-item mb-5 mb-lg-0">
							<div class="feature-icon mb-4">
								<i class="icofont-location-pin"></i>
							</div>
							<span>Servicio en el &aacute;rea metropolitana</span>
							<h4 class="mb-3">Atenci&oacute;n a domicilio</h4>
							<p>Vamos a tu hogar o lugar de trabajo.<br>Servicio disponible en el &aacute;rea metropolitana de la CDMX.</p>
						</div>

						<div class="feature-item mb-5 mb-lg-0">
							<div class="feature-icon mb-4">
								<i class="icofont-ui-clock"></i>
							</div>
							<span>Recolecci&oacute;n agendada</span>
							<h4 class="mb-3">Horarios de recolecci&oacute;n</h4>
							<ul class="w-hours list-unstyled">
								<li class="d-flex justify-content-between">Lun - Vie : <span>9:00am - 5:00pm</span></li>
								<li class="d-flex justify-content-between">S&aacute;bado: <span>9:00am - 1:00pm</span></li>
							</ul>
							<center><a href="#homeslider" class="btn btn-main-2 btn-round-full" id="btn-reserva-tu-cita">Reserva tu cita</a></center>
						</div>

						<div class="feature-item mb-5 mb-lg-0">
							<div class="feature-icon mb-4">
								<i class="icofont-surgeon-alt"></i>
							</div>
							<span>Asistencia electr&oacute;nica las 24 horas</span>
							<h4 class="mb-3">Estamos para servirte</h4>
							<p class="mb-4">Cont&aacute;ctanos para brindarte informaci&oacute;n personalizada y agendar tu cita. <br>
								<a style="display: none;" id="btn-whatsapp-24hrs" target="_blank" href="https://wa.me/525518239725?text=Buenos%20d%C3%ADas%2C%20me%20interesa%20m%C3%A1s%20informaci%C3%B3n%20sobre%20la%20prueba%20PCR%20de%20coronavirus%20%28COVID-19%29%20por%20favor.">WhatsApp <i class="icofont-brand-whatsapp"></i></a>
							</p>
						</div>

					</div>
				</div>
			</div>
		</div>
	</section> -->
	<section class="texto02">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="feature-item mb-5 mb-lg-0">
						<center>
							<h2>¡Conoce nuestras pruebas!<h2>
						</center>

						<center>
							<table class="d-none d-sm-none d-md-block table-striped">
								<thead>
									<tr class="table title-tab" style="text-align: center; background-color: #223a66; color: white;">
										<th scope="col" style="width: 20%"></th>
										<th scope="col">Prueba PCR</th>
										<th scope="col">Prueba de antígenos</th>
										<th scope="col">Prueba rápida de anticuerpos</th>
										<th scope="col">Prueba IgG</th>

									</tr>
								</thead>
								<tbody class="body-align">
									<tr>
										<th scope="row" style="text-align: right;">Toma de muestras</th>
										<td style="text-align: center;">Hisopado nasofaríngeo</td>
										<td style="text-align: center;">Hisopado nasofaríngeo</td>
										<td style="text-align: center;">Muestra de sangre</td>
										<td style="text-align: center;">Muestra de sangre</td>
									</tr>
									<tr>
										<th scope="row" style="text-align: right;">Se recomienda cuando...</th>
										<td style="text-align: center;">Estuviste en contacto con alguien contagiado o presentas síntomas.</td>
										<td style="text-align: center;">Estuviste en contacto con alguien contagiado o presentas síntomas.</td>
										<td style="text-align: center;">Después de haber presentado un contagio.</td>
										<td style="text-align: center;">Después de haber presentado un contagio.</td>
									</tr>
									<tr>
										<th scope="row" style="text-align: right;">Características<br> de la prueba</th>
										<td style="text-align: center;">Es la mejor para determinar si estás contagiado.</td>
										<td style="text-align: center;">Tiene alto índice de confiabilidad y arroja rápido los resultados.</td>
										<td style="text-align: center;">Mide si tienes anticuerpos frente a la infección</td>
										<td style="text-align: center;">Mide cuántos anticuerpos generaste después de la infección.</td>
									</tr>
									<tr>
										<th scope="row" style="text-align: right;">Tiempo de espera<br> para resultados</th>
										<td style="text-align: center;">24 a 36 horas </td>
										<td style="text-align: center;">15 a 20 minutos</td>
										<td style="text-align: center;">10 minutos</td>
										<td style="text-align: center;">24 horas</td>
									</tr>
									<tr style="background-color: #223a66; color:white">
										<th scope="row" style="text-align: right;">Precio</th>
										<td style="text-align: center;">
											<b>$2,099.00 MXN</b><br>
											*Incluye aplicación
										</td>
										<td style="text-align: center;"><b>$850.00 MXN</b><br>
											*Incluye aplicación
										</td>
										<td style="text-align: center;"><b>$750.00 MXN</b><br>
											*Incluye aplicación
										</td>
										<td style="text-align: center;"><b>$999.00 MXN</b><br>
											*Incluye aplicación
										</td>
									</tr>
								</tbody>
							</table>
							<div class="card-group d-block d-sm-block d-md-none">
								<div class="card">
									<div class="card-header" style="background-color: #223a66;">
										<h3 class="card-title" style="color:white;">Prueba PCR</h5>
									</div>

									<div class="card-body">
										<h6>Toma de muestras</h6>
										<p class="card-text">Hisopado nasofaríngeo</p>
										<h6>Se recomienda cuando... </h6>
										<p class="card-text">Estuviste en contacto con alguien contagiado o presentas síntomas.</p>
										<h6>Características de la prueba</h6>
										<p class="card-text">Es la mejor para determinar si estás contagiado.</p>
										<h6>Tiempo de espera para resultados </h6>
										<p class="card-text">24 a 36 horas</p>
										<div class="card-footer" style="background-color: #223a66;">
											<h6 style="color:white;">Precio</h6>
											<p class="card-text" style="color:white;"><b>$2,099.00 MXN</b><br>
												*Incluye aplicación</p>
										</div>
									</div>
								</div>
								<div class="card">
									<div class="card-header" style="background-color: #223a66;">
										<h3 class="card-title" style="color:white;">Prueba de antígenos</h5>
									</div>

									<div class="card-body">
										<h6>Toma de muestras</h6>
										<p class="card-text">Hisopado nasofaríngeo </p>
										<h6>Se recomienda cuando... </h6>
										<p class="card-text">Estuviste en contacto con alguien contagiado o presentas síntomas.</p>
										<h6>Características de la prueba</h6>
										<p class="card-text">Tiene alto índice de confiabilidad y arroja rápido los resultados.</p>
										<h6>Tiempo de espera para resultados </h6>
										<p class="card-text">15 a 20 minutos</p>
										<div class="card-footer" style="background-color: #223a66;">
											<h6 style="color:white;">Precio</h6>
											<p class="card-text" style="color:white;"><b>$850.00 MXN</b><br>
												*Incluye aplicación</p>
										</div>

									</div>

								</div>
								<div class="card">
									<div class="card-header" style="background-color: #223a66;">
										<h3 class="card-title" style="color:white;">Prueba rápida de anticuerpos</h3>
									</div>
									<div class="card-body">
										<h6>Toma de muestras</h6>
										<p class="card-text">Muestra de sangre</p>
										<h6>Se recomienda cuando... </h6>
										<p class="card-text">Después de haber presentado un contagio.</p>
										<h6>Características de la prueba</h6>
										<p class="card-text">Mide si tienes anticuerpos frente a la infección</p>
										<h6>Tiempo de espera para resultados</h6>
										<p class="card-text">10 minutos</p>
										<div class="card-footer" style="background-color: #223a66;">
											<h6 style="color:white;">Precio</h6>
											<p class="card-text" style="color:white;"><b>$750.00 MXN</b><br>
												*Incluye aplicación</p>
										</div>
									</div>

								</div>
								<div class="card">
									<div class="card-header" style="background-color: #223a66;">
										<h3 class="card-title" style="color:white;">Prueba IgG</h3>
									</div>
									<div class="card-body">
										<h6>Toma de muestras</h6>
										<p class="card-text">Muestra de sangre</p>
										<h6>Se recomienda cuando... </h6>
										<p class="card-text">Después de haber presentado un contagio.</p>
										<h6>Características de la prueba</h6>
										<p class="card-text">Mide cuántos anticuerpos generaste después de la infección.</p>
										<h6>Tiempo de espera para resultados </h6>
										<p class="card-text">24 horas</p>
										<div class="card-footer" style="background-color: #223a66;">
											<h6 style="color:white;">Precio</h6>
											<p class="card-text" style="color:white;"><b>$999.00 MXN</b><br>
												*Incluye aplicación</p>

										</div>
									</div>

								</div>
							</div>
						</center>

						<br>
						<!-- <center><a href="#homeslider" class="btn btn-main-2 btn-round-full" id="btn-mas-informacion">MÁS INFORMACIÓN</a></center> -->
					</div>
				</div>
			</div>
		</div>
	</section>

	<section class="texto01">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="feature-item mb-5 mb-lg-0">
						<center>
							<h2>Prueba PCR con toma de muestra a domicilio<h2>
						</center>

						<p>¡Agenda una cita con nosotros! Garantizamos tener tus resultados en un lapso de <b>24 a 36 horas</b> después de la recolección de tu muestra.</p>


						<center><a href="#homeslider" class="btn btn-main-2 btn-round-full" id="btn-agendar-cita">AGENDAR CITA</a></center>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section class="texto02">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="feature-item mb-5 mb-lg-0">
						<center>
							<h2>Precio especial para empresas<h2>
						</center>

						<center>
							<p>Trabaja bajo las normativas del Gobierno de CDMX. Por disposición oficial las empresas deberán realizar pruebas PCR mensuales al 10% de sus empleados.<br><br>
								Pregunte por nuestros paquetes preferenciales.<br>
							</p>
						</center>


						<center><a href="#homeslider" class="btn btn-main-2 btn-round-full" id="btn-mas-informacion">MÁS INFORMACIÓN</a></center>
					</div>
				</div>
			</div>
		</div>
	</section>

	<!---<section class="texto03">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="feature-item mb-5 mb-lg-0">
						<center>
							<h2>¡Promociones de fin de año! <h2>
						</center>

						<center>
							<p><b>Prueba PCR</b> de <b>$2,999.00 a tan sólo $2,599.00</b>, paga ahora y úsala cuando tu quieras en <b>noviembre o diciembre 2020</b></p>
						</center>


						<center><a href="#homeslider" class="btn btn-main-2 btn-round-full" id="btn-preguntar-ahora">PREGUNTAR AHORA</a></center>
					</div>
				</div>
			</div>
		</div>
	</section>--->

	<!-- footer Start -->
	<footer class="footer section gray-bg" style="padding:0">
		<div class="container">
			<div class="footer-btm py-4 mt-5">
				<div class="row align-items-center justify-content-between">
					<div class="col-lg-4">
						<div class="copyright">
							&copy; 2020 PCR Investigation &amp; Reference
						</div>
					</div>
					<div class="col-lg-4 text-center">

					</div>
					<div class="col-lg-4">
						<div class="subscribe-form text-lg-right mt-5 mt-lg-0">
							<!--form action="#" class="subscribe"-->
							<!--input type="text" class="form-control" placeholder="Your Email address"-->
							<a href="../privacidad.html" class="btn btn-outline-info" role="button">Aviso de privacidad</a>
							</form>
						</div>
					</div>
				</div>

				<div class="row">
					<div class="col-lg-4">
						<!--a class="backtop js-scroll-trigger" href="../#top"--><a class="backtop js-scroll-trigger" target="_blank" href="https://wa.me/525518239725?text=Buenos%20d%C3%ADas%2C%20me%20interesa%20m%C3%A1s%20informaci%C3%B3n%20sobre%20la%20prueba%20PCR%20de%20coronavirus%20%28COVID-19%29%20por%20favor.">
							<!--i class="icofont-long-arrow-up"></i--><i class="icofont-brand-whatsapp"></i>
						</a>
					</div>
				</div>
			</div>
		</div>
	</footer>


	<!-- Modal -->
	<div class="modal fade" id="staticBackdrop" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">

				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>

				<div class="modal-body">
					<p><?php
						if (isset($error)) {
							echo $error;
						}
						?>
					</p>

				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
				</div>
			</div>
		</div>
	</div>
	<section class="d-none justify-content-center align-items-center loading" id="loading-page">
		<img src="assets/img/loading.gif" alt="" class="loading-img img-fluid">
		<p>Loading...</p>
	</section>



	<!-- 
    Essential Scripts
    =====================================-->


	<!-- Main jQuery -->
	<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.2/dist/jquery.validate.min.js"></script>
	<!-- Bootstrap 4.3.2 -->
	<script src="../plugins/bootstrap/js/popper.js"></script>
	<script src="../plugins/bootstrap/js/bootstrap.min.js"></script>
	<script src="../plugins/counterup/jquery.easing.js"></script>
	<!-- Slick Slider -->
	<script src="../plugins/slick-carousel/slick/slick.min.js"></script>
	<!-- Counterup -->
	<script src="../plugins/counterup/jquery.waypoints.min.js"></script>

	<script src="../plugins/shuffle/shuffle.min.js"></script>
	<script src="../plugins/counterup/jquery.counterup.min.js"></script>
	<!-- Google Map -->
	<!--script src="../plugins/google-map/map.js"></script>
    <script src="../https://maps.googleapis.com/maps/api/js?key=AIzaSyAkeLMlsiwzp6b3Gnaxd86lvakimwGA6UA<script src="../https://maps.googleapis.com/maps/api/js?key=AIzaSyAkeLMlsiwzp6b3Gnaxd86lvakimwGA6UA&callback=initMap"></script>callback=initMap"></script-->

	<!-- script src="../js/script.js"></script>
    <script src="../js/contact.js"></script-->

	<!-- -->
	<!-- The core Firebase JS SDK is always required and must be listed first -->
	<script src="https://www.gstatic.com/firebasejs/7.21.0/firebase-app.js"></script>

	<!-- TODO: Add SDKs for Firebase products that you want to use
     https://firebase.google.com/docs/web/setup#available-libraries -->
	<script src="https://www.gstatic.com/firebasejs/7.21.0/firebase-analytics.js"></script>

	<script src="https://www.gstatic.com/firebasejs/7.21.0/firebase-auth.js"></script>
	<script src="https://www.gstatic.com/firebasejs/7.21.0/firebase-database.js"></script>
	<script src="assets/js/form.js?v=<?= rand(); ?>"></script>
	<?php
	if (isset($error)) {
		echo "<script>$('#staticBackdrop').modal('show')</script>";
	}
	?>
	<!-- -->

</body>

</html>