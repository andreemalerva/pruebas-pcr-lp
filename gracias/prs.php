<!DOCTYPE html>
<html lang="es">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="description" content="Pruebas COVID-19 en Mexico con recoleccion a domicilio y pago con tarjeta o deposito en OXXO">
    <meta name="author" content="SAFE Mexico">
    <link rel="canonical" href="https://covidtesting.mx">
    <meta name="robots" content="index, follow">
    <meta name=viewport content="width=device-width, initial-scale=1">

    <title>Análisis COVID-19 a domicilio</title>

    <!-- Favicon -->
    <link rel="shortcut icon" type="image/x-icon" href="../../images/favicon.ico" />

    <!-- bootstrap.min css -->
    <link rel="stylesheet" href="../../plugins/bootstrap/css/bootstrap.min.css">
    <!-- Icon Font Css -->
    <link rel="stylesheet" href="../../plugins/icofont/icofont.min.css">
    <!-- Slick Slider  CSS -->
    <link rel="stylesheet" href="../../plugins/slick-carousel/slick/slick.css">
    <link rel="stylesheet" href="../../plugins/slick-carousel/slick/slick-theme.css">

    <!-- Main Stylesheet -->
    <link rel="stylesheet" href="../../css/style.css">



    <!-- End Facebook Pixel Code -->

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-47668237-2"></script>
    <script>
        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments);
        }
        gtag('js', new Date());
        gtag('config', 'UA-47668237-2');
    </script>
    <!-- Google Tag Manager -->
    <script>
        (function(w, d, s, l, i) {
            w[l] = w[l] || [];
            w[l].push({
                'gtm.start': new Date().getTime(),
                event: 'gtm.js'
            });
            var f = d.getElementsByTagName(s)[0],
                j = d.createElement(s),
                dl = l != 'dataLayer' ? '&l=' + l : '';
            j.async = true;
            j.src =
                'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
            f.parentNode.insertBefore(j, f);
        })(window, document, 'script', 'dataLayer', 'GTM-WGBTV8D');
    </script>
    <!-- End Google Tag Manager -->


    <style type="text/css">
        .banner {
            min-height: 650px !important;
        }

        .input100 {
            width: 100%;
        }

        h1,
        label {
            color: #223a66;
        }

        h2 {
            font-size: 27px;
            color: #223a66;
        }

        .form-container {
            background: #fff;
            z-index: 100;
            position: relative;
            padding: 15px;
            margin: 10px;
            border-radius: 15px 15px 15px 15px;
        }

        .texto01,
        .texto02,
        .texto03 {
            margin-top: 50px;
        }

        .carousel-control-prev {
            left: -18px;
        }

        .carousel-control-next {
            right: -18px;
        }

        .banner .block h1 {
            color: #fff;
        }

        .txtColorSlider {
            color: #fff;
        }

        .banner .block {
            padding: 0px 20px 20px 20px;
        }

        .error {
            margin-bottom: 0px;
            padding: 0px;
            font-size: 10px;
            color: #e12454;
        }

        label {
            margin: 0px;
        }

        @media (max-width: 480px) {
            .banner {
                background: url(https://covidtesting.mx/images/bg/slider-bg-1.jpg) no-repeat !important;
                background-size: cover !important;
                background-position: center !important;
            }
        }

        @media (max-width: 400px) {
            .banner {
                background: url(https://covidtesting.mx/images/bg/slider-bg-1.jpg) no-repeat !important;
                background-size: cover !important;
                background-position: center !important;
            }
        }

        @media (max-width: 768px) {
            .banner {
                background: url(https://covidtesting.mx/images/bg/slider-bg-1.jpg) no-repeat !important;
                background-size: cover !important;
                background-position: center !important;
            }
        }

        @media (max-width: 992px) {
            .banner {
                background: url(https://covidtesting.mx/images/bg/slider-bg-1.jpg) no-repeat !important;
                background-size: cover !important;
                background-position: center !important;
            }
        }
    </style>
</head>

<body id="top">
    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-WGBTV8D" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->

    <header>
        <div class="header-top-bar">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-lg-6">

                    </div>
                    <div class="col-lg-6">
                        <div class="text-lg-right top-right-bar mt-2 mt-lg-0">

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <nav class="navbar navbar-expand-lg navigation" id="navbar">
            <div class="container">
                <a class="navbar-brand" href="#">
                    <img src="../../images/logo.png" alt="PCR Investigation and Reference LOGO" class="img-fluid">
                </a>

                <button class="navbar-toggler collapsed" type="button" data-toggle="collapse" data-target="#navbarmain" aria-controls="navbarmain" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="icofont-navigation-menu"></span>
                </button>


        </nav>
    </header>




    <!-- Slider Start -->
    <section class="banner">
        <div class="container">
            <div id="homeslider" class="row carousel slide" data-ride="carousel">
                <div class="col-lg-6 col-md-6 col-xl-6 carousel-inner txtColorSlider">
                    <br>
                    <h1>¡Gracias por contactarnos!</h1>
                    <p style="    color: #223a66;">Con tu información un técnico se comunicará contigo para más detalles sobre tu prueba.</p>
                    <p style="    color: #223a66;">Envíanos un WhatsApp para agendar tu cita.</p>
                    <a href="https://bit.ly/3rHgcHG" class="btn btn-main-2 btn-round-full" id="btn-agendar-cita">ENVIAR</a>
                </div>
                <!---<div class="col-lg-6 col-md-6 col-xl-6" style=" height: 650px;">
                
            </div>--->
            </div>


        </div>
    </section>





    <!-- footer Start -->
    <footer class="footer section gray-bg" style="padding:0">
        <div class="container">
            <div class="footer-btm py-4 mt-5">
                <div class="row align-items-center justify-content-between">
                    <div class="col-lg-4">
                        <div class="copyright">
                            &copy; 2020 PCR Investigation &amp; Reference
                        </div>
                    </div>
                    <div class="col-lg-4 text-center">

                    </div>
                    <div class="col-lg-4">
                        <div class="subscribe-form text-lg-right mt-5 mt-lg-0">
                            <!--form action="#" class="subscribe"-->
                            <!--input type="text" class="form-control" placeholder="Your Email address"-->
                            <a href="../privacidad.html" class="btn btn-outline-info" role="button">Aviso de privacidad</a>
                            </form>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-4">
                        <!--a class="backtop js-scroll-trigger" href="../#top"--><a class="backtop js-scroll-trigger" target="_blank" href="https://wa.me/525518239725?text=Buenos%20d%C3%ADas%2C%20me%20interesa%20m%C3%A1s%20informaci%C3%B3n%20sobre%20la%20prueba%20PCR%20de%20coronavirus%20%28COVID-19%29%20por%20favor.">
                            <!--i class="icofont-long-arrow-up"></i--><i class="icofont-brand-whatsapp"></i>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </footer>
</body>

</html>